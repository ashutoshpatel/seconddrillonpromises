// import the fs-problem1 files function
const obj = require('../fs-problem1');

// Create a directory of random JSON files
obj.createJsonFiles()
.then( (createdFilesName) => {
    // Delete those files simultaneously 
    // if files are created then now delete the files
    return obj.deleteJsonFiles(createdFilesName);
})
.then( (result) => {
    console.log(result);
})
.catch( (error) => {
    console.log(error);
})