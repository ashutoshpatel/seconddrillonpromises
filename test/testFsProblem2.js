const { error } = require('console');
// import the fs-problem2 files function
const obj = require('../fs-problem2');

// Read the given file lipsum.txt
obj.readTheFile('../lipsum.txt')
.then( (data) => {
    console.log("Read the lipsum file :",data);
    console.log('\n');

    // Convert the content to uppercase & write to a new file.
    return obj.convertUpperCase('../uppercase.txt', data);
})
.then( (result) => {
    console.log(result);
    console.log('\n');
})
.then( () => {
    // Read the new file
    return obj.readTheFile('../uppercase.txt');
})
.then( (data) => {
    console.log("Read the uppercase file :",data);
    console.log('\n');

    // convert it to lower case. Then split the contents into sentences.
    return obj.covertFileInLowerCaseWithSplit('../lowercase.txt',data);
})
.then( (result) => {
    console.log(result);
    console.log('\n');
})
.then( () => {
    // Read the new files
    return obj.readTheFile('../lowercase.txt');
})
.then( (data) => {
    console.log("Read the lowercase file :", data);
    console.log('\n');
    
    // sort the content, write it out to a new file.
    return obj.sortTheContentOfFile('../sortedcontent.txt',data);
})
.then( (result) => {
    console.log(result);
    console.log('\n');
})
.then( () => {
    // Read the contents of filenames.txt
    return obj.readTheFile('../filenames.txt');
})
.then( (data) => {
    console.log("Read the filenames.txt :", data);
    console.log('\n');
    
    // delete all the new files that are mentioned in that list simultaneously.
    return obj.deleteTheFiles(data);
})
.then( (result) => {
    console.log(result);
})
.catch( (error) => {
    console.log(error);
});