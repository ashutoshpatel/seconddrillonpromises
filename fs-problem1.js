const { error } = require('console');
const { resolve } = require('path');

const fs = require('fs').promises;

const directoryName = '../random_json_file';
const numberOfFiles = 3;

function createJsonFiles(){

    return fs.mkdir(directoryName)
    .then( () => {

        // create array to store file names
        const createdFilesName = [];

        // use for loop to create n number of files
        for(let index=1; index<=numberOfFiles; index++){
            const fileName = `file_${index}.json`;
            const fileContent = { data : Math.random()};
            const path = directoryName + "/" + fileName;

            // create a file using fs.writeFile
            fs.writeFile(path, JSON.stringify(fileContent))
            .catch( (error) => {
                throw new Error("Error to creating files :",error);
            });

            // add created file name in createdFilesName array
            createdFilesName.push(path);
        }

        return Promise.all(createdFilesName);
    })
    .catch( (error) => {
        throw new Error("Error to create directory :", error);
    });
}

function deleteJsonFiles(files){

    // iterate on files to get file one by one
    files.forEach( (currentFile) => {
        // delete the current file
        fs.unlink(currentFile)
        .catch( (error) => {
            throw new Error("Error to deleting files :", error)
        })
    });

    // delete the directory also
    fs.rmdir(directoryName)
    .catch( (error) => {
        throw new Error("Error to deleting directory :", error);
    });

    // if files deleted successfully
    return Promise.resolve("Files Deleted SuccessFully");
}

// export the functions
module.exports = {
    createJsonFiles,
    deleteJsonFiles,
}