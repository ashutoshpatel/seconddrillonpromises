const { error } = require('console');

const fs = require('fs').promises;

function readTheFile(path){

    return fs.readFile(path, 'utf-8')
    .then( (data) => {
        return Promise.resolve(data);
    })
    .catch( (error) => {
        throw new Error(`Error to read ${path.slice(3)} file :`, error);
    })
}

function convertUpperCase(path, data){

    const upperCaseData = data.toUpperCase();

    return fs.writeFile(path, upperCaseData)
    .then( () => {
        // Store the name of the new file in filenames.txt
        fs.appendFile('../filenames.txt', path+'\n')
        .catch( (error) => {
            throw new Error("Error to add file path in filenames.txt :", error);
        });

        // if the file is created successfully return success message
        return Promise.resolve(`The ${path.slice(3)} file is created successfully.`);
    })
    .catch( (error) => {
        throw new Error(`Error to create ${path.slice(3)} :`, error);
    });
}

function covertFileInLowerCaseWithSplit(path, data){

    const dataOfArray = data.toLowerCase().split('.');
    const newData = dataOfArray.join('\n');

    return fs.writeFile(path, newData)
    .then( () => {
        // Store the name of the new file in filenames.txt
        fs.appendFile('../filenames.txt', path+'\n')
        .catch( (error) => {
            throw new Error("Error to add file path in filenames.txt :", error);
        });

        // if the file is created successfully return success message
        return Promise.resolve(`The ${path.slice(3)} file is created successfully.`);
    })
    .catch( (error) => {
        throw new Error(`Error to create ${path.slice(3)} :`, error);
    });
}

function sortTheContentOfFile(path, data){

    const dataOfArray = data.split('.');
    dataOfArray.sort();

    return fs.writeFile(path, dataOfArray.join('\n'))
    .then( () => {
        // Store the name of the new file in filenames.txt
        fs.appendFile('../filenames.txt', path+'\n')
        .catch( (error) => {
            throw new Error("Error to add file path in filenames.txt :", error);
        });

        // if the file is created successfully return success message
        return Promise.resolve(`The ${path.slice(3)} file is created successfully.`);
    })
    .catch( (error) => {
        throw new Error(`Error to create ${path.slice(3)} :`, error);
    });
}

function deleteTheFiles(files){

    const filesOfArray = files.trim().split('\n');

    // iterate on files to get file one by one
    filesOfArray.forEach( (currentFile) => {
        // delete the current file
        fs.unlink(currentFile)
        .catch( (error) => {
            throw new Error(`Error to deleting files ${currentFile} this :`, error);
        });
    })

    // delete the filenames.txt also
    fs.unlink('../filenames.txt')
    .catch( (error) => {
        throw new Error("Error to deleting filenames.txt :", error);
    });

    // if files deleted successfully
    return Promise.resolve("Files Deleted SuccessFully");
}

// export the functions
module.exports = {
    readTheFile,
    convertUpperCase,
    covertFileInLowerCaseWithSplit,
    sortTheContentOfFile,
    deleteTheFiles,
}